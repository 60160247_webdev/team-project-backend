const express = require('express')
const router = express.Router()
const usersController = require('../controller/UsersController')
const verifyAddNewUser = require('../middlewares/verifyAddNewUser')

router.post('/login', usersController.login)

router.get('/', usersController.getUsers)

router.get('/:id', usersController.getUser)

router.post(
  '/',
  [verifyAddNewUser.checkDuplicateUsernameOrEmail],
  usersController.addUser
)

router.put('/', usersController.updateUser)

router.delete('/:id', usersController.deleteUser)

module.exports = router
