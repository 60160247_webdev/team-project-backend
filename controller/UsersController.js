const User = require('../models/User')
const config = require('../auth.config')
const jwt = require('jsonwebtoken')

const usersController = {
  async login (req, res, next) {
    const payload = req.body
    try {
      await User.findOne({
        username: payload.username,
        password: payload.password
      }).exec((err, user) => {
        if (err) {
          res.status(500).send({ message: err })
        }

        if (user === null) {
          return res
            .status(404)
            .send({ message: 'Username or Password is incorrect.' })
        }

        var token = jwt.sign({ id: user._id }, config.secret, {
          expiresIn: 86400 // 24 hours
        })
        res.json({
          _id: user._id,
          username: user.username,
          email: user.email,
          accessToken: token
        })
      })
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addUser (req, res, next) {
    const payload = req.body
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      await User.updateOne({ _id: payload._id }, payload)
      res.json(payload)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = usersController
