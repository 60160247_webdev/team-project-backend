const Product = require('../models/Product')
const productsController = {
  productList: [],
  lastId: 3,
  async addProduct (req, res, next) {
    const payload = req.body
    const product = new Product(payload)
    try {
      await product.save()
      res.json(product)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateProduct (req, res, next) {
    const payload = req.body
    try {
      await Product.updateOne({ _id: payload._id }, payload)
      res.json(payload)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteProduct (req, res, next) {
    const { id } = req.params
    try {
      const product = await Product.deleteOne({ _id: id })
      res.json(product)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getProducts (req, res, next) {
    try {
      const products = await Product.find({})
      res.json(products)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getProduct (req, res, next) {
    const { id } = req.params
    try {
      const product = await Product.findById(id)
      res.json(product)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = productsController
