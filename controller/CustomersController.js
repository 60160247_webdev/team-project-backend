const Customer = require('../models/Customer')
const customersController = {
  customerList: [],
  lastId: 3,
  async addCustomer (req, res, next) {
    const payload = req.body
    const customer = new Customer(payload)
    try {
      await customer.save()
      res.json(customer)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateCustomer (req, res, next) {
    const payload = req.body
    try {
      await Customer.updateOne({ _id: payload._id }, payload)
      res.json(payload)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCustomer (req, res, next) {
    const { id } = req.params
    try {
      const customer = await Customer.deleteOne({ _id: id })
      res.json(customer)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getCustomers (req, res, next) {
    try {
      const customers = await Customer.find({})
      res.json(customers)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCustomer (req, res, next) {
    const { id } = req.params
    try {
      const customer = await Customer.findById(id)
      res.json(customer)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = customersController
