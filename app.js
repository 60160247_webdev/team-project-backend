var express = require('express')
var cors = require('cors')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const bodyParser = require('body-parser')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var customersRouter = require('./routes/customers')
var productsRouter = require('./routes/products')
require('./connect_users')
require('./connect_customers')
require('./connect_products')

var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// const PORT = process.env.PORT || 8080
// app.listen(PORT, () => {
//   console.log(`Server is running on port ${PORT}.`)
// })

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/customers', customersRouter)
app.use('/products', productsRouter)

module.exports = app
